from duneweb.models import NFT
from duneweb.serializers import NFTSerializer
from rest_framework.renderers import JSONRenderer

# python3 manage.py runscript serializer_test    

my_queryset = NFT.objects.all()[0]
print (my_queryset) #complex dataset (web applications doesnt accept this) / only accepts xml or json

serializer = NFTSerializer()

my_ordered_dict = serializer.to_representation(my_queryset)
print (type(my_ordered_dict))

#instance responsible for serializing dictionary to json (bytes object)
renderer=JSONRenderer()
my_serialized_dict=renderer.render(my_ordered_dict)
print(my_serialized_dict)
