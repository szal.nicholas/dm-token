"""DuneMoon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from duneweb import views

from django.conf import settings
from django.conf.urls.static import static

from duneweb import api_views


urlpatterns = [
    path('api/nft_list/', api_views.NFTList.as_view()), #this allows you to look at your json list of content via 
    path('api/nft_list/2days', api_views.LastNFTList.as_view()), #this allows you to look at your json list of content via 
    path('api/nft_list/new', api_views.NFTCreate.as_view()), #allows user to create something through the usage of your api
    path('api/nft_list/<str:name>/', api_views.NFTRetrieveUpdateDestroy.as_view()), #allows user to create something through the usage of your api

    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('token_detail/<str:token_name>/', views.token_detail, name='token_detail'),
    path('tokens/', views.tokens_all, name='tokens_all'), #brute URL, path converter
    path('vision/', views.vision_all, name='vision_all'),
    path('members/', views.members_all, name='members_all'),
    path('road/', views.road_all, name='road_all'),
    path('nft/', views.nft_all, name='nft_all'),
    #below line of code is for serving static files during development only
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
