from django import forms
from .models import NFT

class NftForm(forms.ModelForm):
    class Meta:
        model=NFT
        fields=['name','description']
        labels={'name':'NFT Name', 'description':'Description'}


