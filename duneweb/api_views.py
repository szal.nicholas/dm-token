#it a

from rest_framework.generics import DestroyAPIView, ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView
from duneweb.serializers import NFTSerializer
from duneweb.models import NFT
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.pagination import LimitOffsetPagination
from django.utils import timezone
from datetime import timedelta
from django.core.cache import cache

class NFTPagination(LimitOffsetPagination):
    defualt_limit = 10
    max_limit = 100


class NFTList(ListAPIView):
    queryset = NFT.objects.all()
    serializer_class = NFTSerializer 
    filter_backends = (DjangoFilterBackend, SearchFilter) #makes sure the comma is there, dont ask me why.
    filter_fields = ('name',) #this will enable filtering by specific fields in the data. will add the field to the URL
    search_fields = ('name', 'description') #used by the SearchFilter backend to map from the query field parameters to model fields of serialized model.
    pagination_class = NFTPagination #add support for limit and offset(page number) url query parameters
class LastNFTList(NFTList):

    def get_queryset(self):
        return NFTList.queryset.filter( #
            date__lte = timezone.now(), #time lower than or equal (date is the name of the column)
            date__gte = timezone.now()-timedelta(days=2)#time greater than or equal
        )

#allows user to create something new through the usage of your api
class NFTCreate(CreateAPIView):
    serializer_class = NFTSerializer

    def create(self, request, *args, **kwargs):
        #validation goes here. This class isnt doing anything right now just a placeholder
        return super().create(request, *args, **kwargs)

# curl -X POST http://localhost:8000/api/nft_list/new -d name='stupid monkey NFT' -d description='This NFT is worthless'

class NFTRetrieveUpdateDestroy(RetrieveUpdateDestroyAPIView):
    queryset = NFT.objects.all()
    lookup_field='name'
    serializer_class=NFTSerializer

    def delete(self,request, *args, **kwargs):
        nft_name = request.data.get('id')
        response = super().delete(request,*args,**kwargs)
        if response.status_code ==204:

            cache.delete(f'nft_data_{nft_name}')
        return response

    def update(self,request, *args, **kwargs):
        response = super().update(request,*args,**kwargs)
        if response.status_code ==200:
            nft = response.data
            cache.set(f"nft_data_{nft['name']}"),{ #TODO replace with ID in the future bc it needs to be unique
                'description': nft['description'],
            }
        return response