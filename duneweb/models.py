from django.db import models
from PIL import Image

# Create your models here.

class Token(models.Model):
    POSITION = [('BUY','BUY'), ('SELL','SELL')]
    token = models.CharField(max_length=100, blank=True)
    ticker = models.CharField(max_length=100, blank=True)
    position = models.CharField(max_length=6, choices=POSITION, blank=True)
    token_image = models.ImageField(upload_to='media')

    def __str__(self):
        return self.token

class Member(models.Model):
    member = models.CharField(max_length=50, default='')
    image = models.ImageField(upload_to='media')
    bio = models.CharField(max_length=400, default='')

# This will crop the picture before saving
    def save(self, *args, **kwargs):
        super().save()
        img = Image.open(self.image.path)
        width, height = img.size  # Get dimensions

        if width > 300 and height > 300:
            # keep ratio but shrink down
            img.thumbnail((width, height))

        # check which one is smaller
        if height < width:
            # make square by cutting off equal amounts left and right
            left = (width - height) / 2
            right = (width + height) / 2
            top = 0
            bottom = height
            img = img.crop((left, top, right, bottom))

        elif width < height:
            # make square by cutting off bottom
            left = 0
            right = width
            top = 0
            bottom = width
            img = img.crop((left, top, right, bottom))

        if width > 300 and height > 300:
            img.thumbnail((300, 300))

        img.save(self.image.path)
    
    def __str__(self):
        return self.member

class Vision(models.Model):
    vision = models.CharField(max_length=5000, default='')
    vision_image = models.ImageField(upload_to='media')
    
    def __str__(self):
        return self.vision

class Road(models.Model):
    road_map = models.ImageField(upload_to='media')
    
    def __str__(self):
        return self.road_map

class NFT(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True)
    name = models.CharField(max_length=50, default='', blank=False) #TODO make sure people cant post without a name
    description = models.TextField(max_length=200, default='(NFT description goes here)')
#check for serializers.py - may remove later
    def is_submitted(self):
        # try:
            # NFT.objects.get(name=self.name)
        #     return True
        # except NFT.DoesNotExist:
        #     return False
        if NFT.objects.filter(name=self.name).exists():
            return True
        else:
            return False
            


