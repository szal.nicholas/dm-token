import csv
from django.core.management import BaseCommand
from duneweb.models import Token

ALREADY_LOADED_ERRORMESSAGE = """
If you need to reload the data from CSV file first delete the db.sqlite3 file to destroy the database.
then run 'python manage.py for a new database with tablets"""

class Command(BaseCommand):
    def handle(self,*args,**kwargs):
        if Token.objects.exists():
            print('Loaded data already loaded')
            print(ALREADY_LOADED_ERRORMESSAGE)
            return
        else:
            # with open('./upload_data.csv') 
            for row in csv.reader(open('./upload_data.csv'),delimiter=','):
                toke=Token()
                toke.token = row [1]
                toke.ticker = row [2]
                toke.position = row [0]
                toke.save()

        

