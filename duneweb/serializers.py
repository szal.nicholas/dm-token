from rest_framework import serializers
from duneweb.models import NFT #NFT post

class NFTSerializer(serializers.ModelSerializer):
    class Meta:
        model = NFT
        fields = ('name', 'description', 'date') #these fields will show up on the api list
# we are overwriting inbuilt method
    def to_representation(self,instance):
        data = super().to_representation(instance)
        data['submitted'] = instance.is_submitted() #points to models.py
        return data
# we are adding custom stuff        
