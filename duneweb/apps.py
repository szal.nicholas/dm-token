from django.apps import AppConfig


class DunewebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'duneweb'
