from django.contrib import admin
from .models import Token, Member, Vision, Road, NFT
from django.forms import TextInput, Textarea
from django.db import models


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    list_display = ['token','ticker','position', 'token_image']

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    member_display = ['member', 'image', 'bio']

@admin.register(Vision)
class VisionAdmin(admin.ModelAdmin):
    vision_display = ['vision', 'vision_image']

@admin.register(Road)
class RoadAdmin(admin.ModelAdmin):
    road_display = 'road_image'

@admin.register(NFT)
class NFTAdmin(admin.ModelAdmin):
    nft_display = ['name','description']