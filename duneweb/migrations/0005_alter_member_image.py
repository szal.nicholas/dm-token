# Generated by Django 3.2.9 on 2021-12-11 20:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('duneweb', '0004_member'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='media'),
        ),
    ]
