from django.shortcuts import render
from django.http import HttpResponse, Http404

from .forms import NftForm
from .models import Token, Member, Vision, Road, NFT


def home(request):
    return render(request=request, template_name='home.html')
    # return HttpResponse('<p>home views</p>')

def tokens_all(request):
    tokens = Token.objects.all() #this links to the table
    return render(request=request, template_name='tokens.html', context={'tokens':tokens})
    # return HttpResponse('<p>home views</p>')
    
def members_all(request):
    members=Member.objects.all()
    return render(request=request, template_name='members.html', context={'members':members})

def vision_all(request):
    visions=Vision.objects.all()
    return render(request=request, template_name='vision.html', context={'visions':visions})
    # return HttpResponse('<p>home views</p>')

def road_all(request):
    roads=Road.objects.all()
    return render(request=request, template_name='road.html', context={'roads':roads})

def nft_all(request):
    #
    if request.method == 'POST': 
        filled_form=NftForm(request.POST) #creates instance for NftForm class with user input data
        if filled_form.is_valid(): #check if form was filled correctly
            filled_form.save()
            note = f"{filled_form.cleaned_data['name']} has been submitted"
            return render(request=request, template_name='nft.html', context={'note':note}) 
        else:
            warning='form incorrectly filled out'
            nft = NFT.objects.all() #this links to the table
            form=NftForm()
            return render(request=request, template_name='nft.html', context={'nft':nft, 'nft_form':form, 'warning':warning}) #this is for get request
    
    else:
        nft = NFT.objects.all() #this links to the table
        form=NftForm()
        return render(request=request, template_name='nft.html', context={'nft':nft, 'nft_form':form}) #this is for get request
    # return HttpResponse('<p>home views</p>')

def token_detail(request, token_name):
    try:
        token = Token.objects.get(token=token_name)
    except Token.DoesNotExist:
        raise Http404 ('not found')
    return render(request=request, template_name='tokens_detail.html', context={'token':token}) #context passes data from the database
    # return HttpResponse(f'<p>We have arrived on Dune</p>')



